from django.core.management.base import BaseCommand

from django_master_data_gtin_manager.django_master_data_gtin_manager.tasks import (
    master_data_gtin_manager_sync_barcodes,
)


class Command(BaseCommand):
    def handle(self, *args, **options):
        master_data_gtin_manager_sync_barcodes()
