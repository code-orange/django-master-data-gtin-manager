# Generated by Django 3.2.13 on 2022-04-30 16:38

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_master_data_gtin_manager", "0004_fix_gtin_manager_gtin_type"),
    ]

    operations = [
        migrations.AlterField(
            model_name="mdatgtinmanagergpc",
            name="gpc",
            field=models.CharField(max_length=13),
        ),
    ]
