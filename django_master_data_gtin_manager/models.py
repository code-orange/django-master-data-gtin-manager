from django.db import models

from django_mdat_customer.django_mdat_customer.models import MdatCustomers, MdatItems


class MdatGtinManagerGpc(models.Model):
    gpc = models.CharField(max_length=13)
    customer = models.ForeignKey(MdatCustomers, models.DO_NOTHING)

    class Meta:
        db_table = "mdat_gtin_manager_gpc"


class MdatGtinManager(models.Model):
    gpc = models.ForeignKey(MdatGtinManagerGpc, models.DO_NOTHING)
    gtin = models.CharField(max_length=13)
    item = models.ForeignKey(MdatItems, models.DO_NOTHING, null=True)

    class Meta:
        db_table = "mdat_gtin_manager"
        ordering = ["gtin"]
