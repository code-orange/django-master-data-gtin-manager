from celery import shared_task
from checkdigit import gs1
from django.conf import settings

from django_master_data_gtin_manager.django_master_data_gtin_manager.models import (
    MdatGtinManagerGpc,
    MdatGtinManager,
)
from django_mdat_customer.django_mdat_customer.models import MdatItems
from django_mdat_prod_info_mgmt.django_mdat_prod_info_mgmt.models import (
    MdatItemBarcodes,
)


@shared_task(name="master_data_gtin_manager_seed")
def master_data_gtin_manager_seed():
    for manager_gpc in MdatGtinManagerGpc.objects.all():
        gtin_prefix_length = len(manager_gpc.gpc)

        # GTIN13 - check digit - existing prefix
        suffix_length = 13 - 1 - gtin_prefix_length

        first_gtin = manager_gpc.gpc

        for i in range(suffix_length):
            first_gtin = first_gtin + "0"

        item_count = 10**suffix_length
        current_gtin = int(first_gtin)

        for i in range(item_count):
            calculated_gtin = str(
                str(current_gtin) + gs1.calculate(str(current_gtin))
            ).zfill(13)

            try:
                manager_gtin = MdatGtinManager.objects.get(
                    gpc=manager_gpc,
                    gtin=calculated_gtin,
                )
            except MdatGtinManager.DoesNotExist:
                manager_gtin = MdatGtinManager.objects.create(
                    gpc=manager_gpc,
                    gtin=calculated_gtin,
                )

            current_gtin += 1

    return


@shared_task(name="master_data_gtin_manager_sync_barcodes")
def master_data_gtin_manager_sync_barcodes():
    for gtin in MdatGtinManager.objects.filter(item__isnull=False):
        try:
            barcode = MdatItemBarcodes.objects.get(item=gtin.item, barcode=gtin.gtin)
        except MdatItemBarcodes.DoesNotExist:
            barcode = MdatItemBarcodes.objects.create(item=gtin.item, barcode=gtin.gtin)

    return


@shared_task(name="master_data_gtin_manager_auto_import")
def master_data_gtin_manager_auto_import():
    my_gpc = MdatGtinManagerGpc.objects.get(id=1)
    item_base_query = MdatItems.objects.filter(
        created_by_id=settings.MDAT_ROOT_CUSTOMER_ID
    ).exclude(mdatgtinmanager__isnull=False)

    to_do = list()

    to_do.extend(list(item_base_query.filter(external_id__startswith="DTELFLAT")))
    to_do.extend(list(item_base_query.filter(external_id__startswith="DTELMIN")))
    to_do.extend(list(item_base_query.filter(external_id__startswith="12DSL")))
    to_do.extend(list(item_base_query.filter(external_id__startswith="12FTTH")))
    to_do.extend(list(item_base_query.filter(external_id__startswith="ENTCONN")))
    to_do.extend(list(item_base_query.filter(external_id__startswith="HSUCPLAN")))
    to_do.extend(list(item_base_query.filter(external_id__startswith="HSEMARC")))
    to_do.extend(list(item_base_query.filter(external_id__startswith="HSEXESS")))
    to_do.extend(list(item_base_query.filter(external_id__startswith="HSPBX")))
    to_do.extend(list(item_base_query.filter(external_id__startswith="HSDOM")))
    # to_do.extend(list(item_base_query.filter(external_id__startswith='HSVM'))) # temporary disable HSVM as >40k items
    to_do.extend(list(item_base_query.filter(external_id__startswith="HSRESVM")))
    to_do.extend(list(item_base_query.filter(external_id__startswith="TL-API")))
    to_do.extend(list(item_base_query.filter(external_id__startswith="TREX-")))

    for item in to_do:
        print(item.name)

        gtins = MdatGtinManager.objects.filter(gpc=my_gpc).filter(item__isnull=True)

        if len(gtins) <= 0:
            continue

        gtin = gtins.first()

        gtin.item = item
        gtin.save()

    return
